function [J, grad] = costFunctionReg(theta, X, y, lambda)

m = length(y); % number of training examples


J = 0;
grad = zeros(size(theta));

%-------------------------------------------------------------
h = sigmoid(theta' * X');
thetaNew = theta(2: length(theta));
J = (1/m)*(-(y)' * log(h)' - (1-y)' * log(1-h)') + (lambda/(2*m))*(thetaNew'*thetaNew);

% Vectorized partial derivatives for all Thetas
temp = theta;
temp(1) = 0;
grad = (1/m)*(X'*(h' - y)) + (lambda/m)*(temp);
grad(1) = (1/m) * ( (h - y')*X(:,1) );


end
