function p = predict(theta, X)

m = size(X, 1); % Number of training examples


p = zeros(m, 1);

%-------------------------------------------------------------
%thetaX = zeros( size(X) );
%for i = 1 : length(thetaX')
%    thetaX(i,:) = theta'.*X(i,:);
%end

p = sigmoid(theta'*X')';

for i = 1 : length(p)
    if p(i) >= 0.5
        p(i) = 1;
    else
        p(i) = 0;
    end
end



end
