function [J, grad] = costFunction(theta, X, y)
    % Initialize some useful values
    m = length(y); % number of training examples

    J = 0;
    grad = zeros(size(theta));

    %--------------------------------------------------------------
    h = sigmoid(theta' * X');
    J = (1/m)*(-(y)' * log(h)' - (1-y)' * log(1-h)');
    
    for i = 1 : length(X(1,:)) 
        grad(i) = (1/m) * ( (h - y')*X(:,i) );
    end

end
