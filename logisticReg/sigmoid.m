function g = sigmoid(z)

g = zeros(size(z));
e = exp(1);

for colNum = 1 : size(z, 2)
    for rowNum = 1 : size(z, 1)
        g(rowNum, colNum) = 1/(1 + e^( -( z(rowNum, colNum) ) ));
    end
end

end
