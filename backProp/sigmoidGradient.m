function g = sigmoidGradient(z)

g = zeros(size(z));

%--------------------------------------------------------------------------
% Notice element wise multiplication
g = sigmoid(z).*(1-sigmoid(z));



end
