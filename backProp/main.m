%% Initialization
clear ; close all; clc

%% Setup the parameters                          
input_layer_size  = 784;  % 28x28 Input Images of Digits
hidden_layer_size = 49;   % 25 hidden units
num_labels = 10;          % 10 classes, from 1 to 10   
                          % "0" to label 10)
% Load Training Data
fprintf('Loading and Visualizing Data ...\n')

X = loadMNISTImages('train-images.idx3-ubyte');
X = X';
y = loadMNISTLabels('train-labels.idx1-ubyte');
% Chnage 0 to 10
for ind = 1 : length(y)
    if y(ind) == 0
        y(ind) = 10;
    end
end

m = size(X, 1);

% Randomly select 100 data points to display
sel = randperm(size(X, 1));
sel = sel(1:100);

%displayData(X(sel, :), 28);


fprintf('\nLoading Saved Neural Network Parameters ...\n')

Theta1 = ones(hidden_layer_size, input_layer_size+1);
Theta2 = ones(num_labels,hidden_layer_size+1);
% Unroll parameters 
nn_params = [Theta1(:) ; Theta2(:)];

%% Compute Cost (Feedforward) 
 
fprintf('\nFeedforward Using Neural Network ...\n')

% Weight regularization parameter (we set this to 0 here).
lambda = 0;

J = nnCostFunction(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, lambda);


%% Implement Regularization 

fprintf('\nChecking Cost Function (w/ Regularization) ... \n')

% Weight regularization parameter (we set this to 1 here).
lambda = 2;

J = nnCostFunction(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, lambda);


%% Sigmoid Gradient  

fprintf('\nEvaluating sigmoid gradient...\n')

g = sigmoidGradient([1 -0.5 0 0.5 1]);
fprintf('Sigmoid gradient evaluated at [1 -0.5 0 0.5 1]:\n  ');
fprintf('%f ', g);
fprintf('\n\n');


%% Initializing Pameters 

fprintf('\nInitializing Neural Network Parameters ...\n')

initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);

% Unroll parameters
initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

%% Implement Backpropagation 

fprintf('\nChecking Backpropagation... \n');

%  Check gradients by running checkNNGradients
checkNNGradients;


%% Implement Regularization 

fprintf('\nChecking Backpropagation (w/ Regularization) ... \n')

%  Check gradients by running checkNNGradients
lambda = 3;
checkNNGradients(lambda);

% Also output the costFunction debugging values
debug_J  = nnCostFunction(nn_params, input_layer_size, ...
                          hidden_layer_size, num_labels, X, y, lambda);



%% Training NN 
fprintf('\nTraining Neural Network... \n')


options = optimset('MaxIter', 400);

%  You should also try different values of lambda
lambda = 0.1;

% Create "short hand" for the cost function to be minimized
costFunction = @(p) nnCostFunction(p, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, X, y, lambda);

% Now, costFunction is a function that takes in only one argument (the
% neural network parameters)
[nn_params, cost] = fmincg(costFunction, initial_nn_params, options);

% Obtain Theta1 and Theta2 back from nn_params
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

save trainedParams.mat Theta1 Theta2 X y;
%% Visualize Weights 

fprintf('\nVisualizing Neural Network... \n')

%displayData(Theta1(:, 2:end), 28);