load('trainedParams.mat')

%displayData(Theta1(:, 2:end), 28);

%displayData(Theta2(:, 2:end), 7);

%Get the test data
X = loadMNISTImages('t10k-images.idx3-ubyte');
X = X';
y = loadMNISTLabels('t10k-labels.idx1-ubyte');
%% Predict 
pred = predict(Theta1, Theta2, X);
%fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);

% Get the image
image = imread('SCVphoto.jpg');
% cast to double
image = im2double(image);
% turn it into grayscale
image = 0.2989 * image(:,:,1) + 0.5870 * image(:,:,2) + 0.1140 * image(:,:,3);
image = image(:)';
% invert the gray scale
image = 1 - image;
displayData(image);

res = predict(Theta1,Theta2,image);

if res == 10
    res = 0;
end

fprintf('I think it is %d \n', res);