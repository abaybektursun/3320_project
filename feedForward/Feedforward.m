clear; 
close all; 
clc;

% Setup the parameters 
% Input Images of Digits
input_layer_size  = 400; 
% hidden units
hidden_layer_size = 25;   
% 10 labels, from 1 to 10  
num_labels = 10;           

% Load Training Data
load('data1.mat');
m = size(X, 1);

% Randomly select 100 data points to display
sel = randperm(size(X, 1));
sel = sel(1:100);
%Display random numbers
displayData(X(sel, :));


% Load the weights into variables Theta1 and Theta2
load('weights.mat');

% # records
m = size(X, 1);
num_labels = size(Theta2, 1);

% Predicted results
out = zeros(size(X, 1), 1);

% Create a bias
X_biased = [ones(size(X,1), 1) X];

% Compute the layer
a_2 = sigmoid(Theta1*X_biased');

% Create a bias
a_2 = [ones(1, size(a_2,2)); a_2];

% Compute the predictions
[value, index] = max(sigmoid(Theta2*a_2));

out = index';


fprintf('\nAccuracy: %f\n', mean(double(out == y)) * 100);


%  To give you an idea of the network's output, you can also run
%  through the examples one at the a time to see what it is predicting.

%  Randomly permute examples
rp = randperm(m);

displayData(X(rp(1), :));

out(rp(1))

%for i = 1:m
%    % Display 
%    fprintf('\nDisplaying Example Image\n');
%    displayData(X(rp(i), :));
%
%    out = predict(Theta1, Theta2, X(rp(i),:));
%    fprintf('\nNeural Network Prediction: %d (digit %d)\n', out, mod(out, 10));
%    
%    % Pause
%    fprintf('Program paused. Press enter to continue.\n');
%    pause;
%end

