from PIL import Image as PILimg
from resizeimage import resizeimage

from SimpleCV import Image, Camera
import os

# Change the working directory
os.chdir(r"../backProp")
# instantiate a camera
cam = Camera()
# take picture
img = cam.getImage()
# save the picture to a file
imageName = 'SCVphoto.jpg'
img.save(imageName)

# resize
with open(imageName, 'r+b') as f:
    with PILimg.open(f) as image:
        cover = resizeimage.resize_cover(image, [28, 28])
        cover.save(imageName, image.format)